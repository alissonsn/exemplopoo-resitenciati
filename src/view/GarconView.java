/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.GarconController;
import repositories.GarconRepository;

/**
 *
 * @author alisson nascimento
 */
public class GarconView extends AbstractView<GarconController>{
    
    public GarconView() {
        super(new GarconController(new GarconRepository()));
    }
    
}
