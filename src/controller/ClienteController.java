/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Cliente;
import repositories.ClienteRepository;

/**
 * @author Alisson Nascimento
 */
public class ClienteController extends AbstractyController<ClienteRepository, Cliente> {
    
    public ClienteController(ClienteRepository clienteRepositprio){
        super(clienteRepositprio);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Cliente item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
