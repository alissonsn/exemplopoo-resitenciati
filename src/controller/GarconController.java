/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Garcon;
import repositories.GarconRepository;

/**
 * @author Alisson Nascimento
 */
public class GarconController extends AbstractyController<GarconRepository, Garcon> {
    
    public GarconController(GarconRepository garconRepository){
        super(garconRepository);
    }
    
    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Garcon item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
