/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import connection.SQLiteDataManipulation;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Item;

/**
 * @author Alisson Nascimento
 */
public class ItemRepository extends AbstractRepository<Item>{
    
    public ItemRepository(){
        super();
    }
    
    @Override
    public void addItem(Item item) {
        item.setId(new Random().nextInt(Integer.MAX_VALUE));
        String sql = "INSERT INTO item(id, descricao, marca, valor) " +
                "VALUES("+ item.getId() + ",'"+ item.getDescricao() +
                "','"+ item.getMarca() + "',"+ item.getValor() + ")";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }
    
    @Override
    public List<Item> getItens() {
        String sql = "SELECT id, descricao, marca, valor FROM item";
        
        List<Item> itens = new ArrayList<>();
        
        try (Connection conn = getConnection();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                Item item = new Item();
                item.setId(rs.getInt("id"));
                item.setDescricao(rs.getString("descricao"));
                item.setMarca(rs.getString("marca"));
                item.setValor(rs.getDouble("valor"));
                itens.add(item);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return itens;
    }
    
    @Override
    public void removeAll() {
        String sql = "DELETE FROM item";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }
}
