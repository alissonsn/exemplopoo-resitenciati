/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ContaController;
import repositories.ContaRepository;

/**
 *
 * @author alisson nascimento
 */
public class ContaView extends AbstractView<ContaController>{
    
    public ContaView() {
        super(new ContaController(new ContaRepository()));
    }
    
}
