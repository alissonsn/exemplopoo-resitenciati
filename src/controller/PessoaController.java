/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Pessoa;
import repositories.PessoaRepository;

/**
 * @author Alisson Nascimento
 */
public class PessoaController extends AbstractyController<PessoaRepository, Pessoa>{
    
    public PessoaController(PessoaRepository pessoaRepository){
        super(pessoaRepository);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Pessoa item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
