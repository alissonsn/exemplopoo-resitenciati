/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import repositories.AbstractRepository;

/**
 * @author Alisson Nascimento
 */
abstract class AbstractyController<T extends AbstractRepository, K>{
    
    private T repository;
    
    public AbstractyController(T repository){
        this.repository = repository;
    }
    
    public void salvar(K item) {
        getRepository().addItem(item);
    }
    
    public void remover(K item) {
        getRepository().removeItem(item);
    }
    
    public void atualizar(K itemOld, K itemNew) {
        getRepository().removeItem(itemOld);
        getRepository().addItem(itemNew);
    }
    
    public List<K> getItens() {
        return getRepository().getItens();
    }
    
    public void removeAll() {
        getRepository().removeAll();
    }
    
    abstract String listar();
    
    public T getRepository() {
        return repository;
    }
    
}
