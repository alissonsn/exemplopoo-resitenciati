/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Conta;
import repositories.ContaRepository;

/**
 * @author Alisson Nascimento
 */
public class ContaController extends AbstractyController<ContaRepository, Conta>{
    public ContaController(ContaRepository contaRepository){
        super(contaRepository);
    }

    @Override
    String listar() {
        String lista = "Lista de contas \n";
        for (Conta item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
