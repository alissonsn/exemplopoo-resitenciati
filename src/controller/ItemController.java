/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import repositories.ItemRepository;

/**
 * @author Alisson Nascimento
 */
public class ItemController extends AbstractyController<ItemRepository, Item>{
    
    public ItemController(ItemRepository itemRepository) {
        super(itemRepository);
    }
    
    @Override
    public String listar() {
        String lista = "Lista de itens \n";
        for (Item item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
