/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Mesa;
import repositories.MesaRepository;

/**
 * @author Alisson Nascimento
 */
public class MesaController extends AbstractyController<MesaRepository, Mesa>{
    
    public MesaController(MesaRepository mesaRepository){
        super(mesaRepository);
    }

    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (Mesa item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
