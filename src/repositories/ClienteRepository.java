/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import connection.SQLiteDataManipulation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Cliente;

/**
 * @author Alisson Nascimento
 */
public class ClienteRepository extends AbstractRepository<Cliente>{
    
    public ClienteRepository(){
        super();
    }

    @Override
    public void addItem(Cliente item) {
        item.setId(new Random().nextInt(Integer.MAX_VALUE));
        String sql = "INSERT INTO cliente(id, nome, cpf, endereco, telefone) " + 
                      "VALUES("+ item.getId() + ",'"+ item.getNome() + 
                                "','"+ item.getCpf() + "','"+ item.getEndereco() + "','"+ item.getTelefone() + "')";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }

    @Override
    public List<Cliente> getItens() {
        String sql = "SELECT id, nome, cpf, endereco, telefone FROM cliente";
        
        List<Cliente> itens = new ArrayList<>();
        
        try (Connection conn = getConnection();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEndereco(rs.getString("endereco"));
                cliente.setCpf(rs.getString("cpf"));
                cliente.setTelefone(rs.getString("telefone"));
                itens.add(cliente);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return itens;
    }

    @Override
    public void removeAll() {
        String sql = "DELETE FROM cliente";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }
    
}
