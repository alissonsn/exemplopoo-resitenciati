/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ClienteController;
import repositories.ClienteRepository;

/**
 *
 * @author alisson nascimento
 */
public class ClienteView extends AbstractView<ClienteController>{
    
    public ClienteView() {
        super(new ClienteController(new ClienteRepository()));
    }
    
}
