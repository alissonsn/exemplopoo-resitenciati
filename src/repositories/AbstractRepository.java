/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import connection.SQLiteJDBCDriverConnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alisson Nascimento
 */
public class AbstractRepository<T> {
    
    private final List<T> itens;
    
    private Connection connection;

    public AbstractRepository() {
        connection = SQLiteJDBCDriverConnection.connect("restaurante.db");
        
        if (connection == null) {
            SQLiteJDBCDriverConnection.createNewDatabase("restaurante.db");
            connection = SQLiteJDBCDriverConnection.connect("restaurante.db");
        }
        
//        String sqlCliente = "DROP TABLE cliente;";
        
        String sqlCliente = "CREATE TABLE IF NOT EXISTS cliente (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	nome text NOT NULL,\n"
                + "	telefone text NOT NULL,\n"
                + "	cpf text NOT NULL,\n"
                + "	endereco text NOT NULL\n"
                + ");";
        
        String sqlGarcon = "CREATE TABLE IF NOT EXISTS garcon (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	nome text NOT NULL,\n"
                + "	telefone text NOT NULL,\n"
                + "	cpf text NOT NULL,\n"
                + "	endereco text NOT NULL\n"
                + ");";
        
        String sqlItem = "CREATE TABLE IF NOT EXISTS item (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	descricao text NOT NULL,\n"
                + "	valor real NOT NULL,\n"
                + "	marca text NOT NULL\n"
                + ");";
        
        SQLiteJDBCDriverConnection.createNewTable(connection, sqlGarcon);
        SQLiteJDBCDriverConnection.createNewTable(connection, sqlCliente);
        SQLiteJDBCDriverConnection.createNewTable(connection, sqlItem);
        
        itens = new ArrayList<>();
    }
    
    public void addItem(T item){ 
        itens.add(item);
    }
    
    public void removeItem(T item) {
        itens.remove(item);
    }
      
    public List<T> getItens(){
        return itens;
    }
    
    public void removeAll() {
        itens.clear();
    }
    
    public Connection getConnection() {
        return connection;
    }
}
