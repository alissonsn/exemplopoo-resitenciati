/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package repositories;

import connection.SQLiteDataManipulation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Garcon;

/**
 * @author Alisson Nascimento
 */
public class GarconRepository extends AbstractRepository<Garcon>{
    public GarconRepository(){
        super();
    }
    
    @Override
    public void addItem(Garcon item) {
        item.setId(new Random().nextInt(Integer.MAX_VALUE));
        String sql = "INSERT INTO garcon(id, nome, cpf, endereco, telefone) " +
                "VALUES("+ item.getId() + ",'"+ item.getNome() +
                "','"+ item.getCpf() + "','"+ item.getEndereco() + "','"+ item.getTelefone() + "')";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }
    
    @Override
    public List<Garcon> getItens() {
        String sql = "SELECT id, nome, cpf, endereco, telefone FROM garcon";
        
        List<Garcon> itens = new ArrayList<>();
        
        try (Connection conn = getConnection();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                Garcon garcon = new Garcon();
                garcon.setId(rs.getInt("id"));
                garcon.setNome(rs.getString("nome"));
                garcon.setEndereco(rs.getString("endereco"));
                garcon.setCpf(rs.getString("cpf"));
                garcon.setTelefone(rs.getString("telefone"));
                itens.add(garcon);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return itens;
    }
    
    @Override
    public void removeAll() {
        String sql = "DELETE FROM garcon";
        SQLiteDataManipulation.execute(getConnection(), sql);
    }
    
}
