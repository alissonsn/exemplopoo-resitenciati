/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ItemController;
import repositories.ItemRepository;

/**
 *
 * @author alisson nascimento
 */
public class ItemView extends AbstractView<ItemController>{
    
    public ItemView() {
        super(new ItemController(new ItemRepository()));
    }
    
}
