/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ItemConta;
import repositories.ItemContaRepository;

/**
 * @author Alisson Nascimento
 */
public class ItemContaController extends AbstractyController<ItemContaRepository, ItemConta>{
    
    public ItemContaController(ItemContaRepository  itemContaRepository){
        super(itemContaRepository);
    }
    
    @Override
    String listar() {
        String lista = "Lista de clientes \n";
        for (ItemConta item : getRepository().getItens()) {
            lista += item + "\n";
        }
        return lista;
    }
}
